package com.telenav.internship;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


class LanguageFinder {
    private final TrainingService trainingService;
    private final BookProcessingJob bookProcessingJob;
    private static final String TESTING_BOOK_PATH = "src/main/resources/testing-books/";

    LanguageFinder(final TrainingService trainingService, final BookProcessingJob bookProcessingJob) {
        this.trainingService = trainingService;
        this.bookProcessingJob = bookProcessingJob;
    }

    Map<String, String> classifyBooks(final List<String> testingBooksNames){

        Map<String, Long> bookWordsAndOccurrences;
        final Map<String, String> identifiedLanguagesForBooks = new HashMap<>();
        String language;
        String pathForBook;
        for (final String testingBookName : testingBooksNames) {
            pathForBook = TESTING_BOOK_PATH + testingBookName;
            bookWordsAndOccurrences = bookProcessingJob.getBookAsWordsAndOccurrences(pathForBook);
            language = trainingService.getLanguageWithHighestResemblanceForBook(bookWordsAndOccurrences);
            System.out.println(language);
            identifiedLanguagesForBooks.put(testingBookName, language);
        }
        return identifiedLanguagesForBooks;

    }
}
