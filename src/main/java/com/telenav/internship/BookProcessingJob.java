package com.telenav.internship;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.Map;


class BookProcessingJob {

    private static final String WORD_REGEXP = "[ .:,!?()_;]+";
    private final SparkSession sparkSession;

    BookProcessingJob(final SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    Map<String, Long> getBookAsWordsAndOccurrences(final String path) {

        final JavaRDD<String> words = JavaSparkContext.fromSparkContext(sparkSession.sparkContext()).textFile(path)
                .flatMap(s -> Arrays.asList(s.split(WORD_REGEXP)).iterator())
                .filter(s -> !s.matches("[.!?_\0-9]"))
                .map(String::toLowerCase);

        return words.countByValue();
    }
}