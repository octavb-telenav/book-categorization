package com.telenav.internship;

import java.util.*;


class TrainingService {

    private final Map<String, Map<String, Long>> languageWithWordsAndOccurrences;
    private final List<String> languages;

    TrainingService() {
        this.languageWithWordsAndOccurrences = new HashMap<>();
        this.languages = new ArrayList<>();
    }

    private void addBookToTrainingSet(final String language, final Map<String, Long> wordsAndOccurrences) {
        this.languageWithWordsAndOccurrences.put(language, wordsAndOccurrences);
        this.languages.add(language);
    }

    void addAllBooksToTrainingSet(final Map<String, Map<String, Long>> books){
        books.forEach(this::addBookToTrainingSet);
    }

    String getLanguageWithHighestResemblanceForBook(final Map<String, Long> bookWords) {
        Double auxResemblance;
        Double maxResemblance = 0.0;
        String language = "";
        for (final String lang : this.languages) {
            auxResemblance = calculateBookAndLanguageAResemblance(bookWords, lang);
            if (auxResemblance > maxResemblance) {
                maxResemblance = auxResemblance;
                language = lang;
            }
        }
        return language;
    }

    private Long getTotalWordsForLanguage(final String language) {
        return this.languageWithWordsAndOccurrences.get(language)
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .mapToLong(Long::longValue)
                .sum();
    }

    private Double getWordFrequencyInLanguage(final String word, final String language) {
        final Long wordOccurrences = this.languageWithWordsAndOccurrences.get(language).get(word);
        if (wordOccurrences == null) {
            return 0.0;
        }
        return (double) wordOccurrences / getTotalWordsForLanguage(language);
    }

    private Double calculateBookAndLanguageAResemblance(final Map<String, Long> bookWords, final String language) {
        return bookWords.entrySet()
                .stream()
                .map(x -> getWordFrequencyInLanguage(x.getKey(), language))
                .mapToDouble(Double::doubleValue)
                .sum();
    }
}