package com.telenav.internship;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


class TrainingSetProvider {
    private final BookProcessingJob bookProcessingJob;

    TrainingSetProvider(final BookProcessingJob bookProcessingJob) {
        this.bookProcessingJob = bookProcessingJob;
    }

    private static final String TRAINING_BOOK_PATH = "src/main/resources/training-books/";

    Map<String, Map<String, Long>> obtainTrainingSet(final List<String> trainingBooksNames){
        Map<String, Long> bookWordsAndOccurrences;
        String pathForBook;
        final Map<String, Map<String, Long>> setOfBooksForTraining = new HashMap<>();

        for (final String trainingBookName : trainingBooksNames) {
            pathForBook = TRAINING_BOOK_PATH + trainingBookName;
            bookWordsAndOccurrences = bookProcessingJob.getBookAsWordsAndOccurrences(pathForBook);
            final String lang = extractBookName(trainingBookName);
            setOfBooksForTraining.put(lang, bookWordsAndOccurrences);
        }
        return setOfBooksForTraining;
    }

    private String extractBookName(final String trainingBookName) {
        return trainingBookName.split("[0-9]")[0];
    }

}
