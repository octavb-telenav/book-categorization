package com.telenav.internship;

import org.apache.spark.sql.SparkSession;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class BookClassification {

    private static final String TRAINING_BOOK_PATH = "src/main/resources/training-books/";
    private static final String TESTING_BOOK_PATH = "src/main/resources/testing-books/";

    public static void main(final String[] args) {

        final SparkSession sparkSession =
                SparkSession.builder().master("local[*]").appName("Book Classification Application").getOrCreate();
        sparkSession.sparkContext().setLogLevel("ERROR");

        final BookProcessingJob bookProcessingService = new BookProcessingJob(sparkSession);
        final TrainingService trainingService = new TrainingService();
        final TrainingSetProvider trainingSetProvider = new TrainingSetProvider(bookProcessingService);

        List<String> trainingBooksNames = new ArrayList<>();
        try {
            trainingBooksNames = getBooksNamesFromPath(TRAINING_BOOK_PATH);
        } catch (final IOException e) {
            System.out.println("Something went wrong when reading your input training files");
            e.printStackTrace();
            System.exit(1);
        }

        trainingService.addAllBooksToTrainingSet(trainingSetProvider.obtainTrainingSet(trainingBooksNames));

        List<String> testingBooksNames = new ArrayList<>();
        try {
            testingBooksNames = getBooksNamesFromPath(TESTING_BOOK_PATH);
        } catch (final IOException e) {
            e.printStackTrace();
            System.out.println("Something went wrong when reading your input testing files");
            System.exit(1);
        }

        final LanguageFinder languageFinder = new LanguageFinder(trainingService, bookProcessingService);

        languageFinder.classifyBooks(testingBooksNames)
                .forEach((key, value) -> System.out.println("BOOK " + key + " IS WRITTEN IN " + value));
    }

    private static List<String> getBooksNamesFromPath(final String path) throws IOException {
        final Stream<Path> paths = Files.walk(Paths.get(path));
        return paths.filter(Files::isRegularFile).filter(x -> x.getFileName().toString().endsWith(".txt"))
                .map(x -> x.getFileName().toString()).collect(Collectors.toList());
    }
}